import { push } from 'connected-react-router'

import store from '../store'

import $         from 'jquery'
import Mousetrap from 'mousetrap'

import './custom.scss'
import 'react-toastify/scss/main.scss'

import 'bootstrap/dist/js/bootstrap.min'

const perform = (last,current=performance.now()) => (
                last ? current-last : () => perform(current)
              )

const p = perform()

$(()=>{
    const delay = p()
    console.log("Document ready, took approx. "+Math.round(delay)+"ms")
    //https://getbootstrap.com/docs/4.1/components/forms/
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });

    $(function () {
      $('[data-toggle="popover"]').popover()
    })

    Mousetrap.bind('shift+r', ()=> ( store.dispatch({type:"GAME_RESET"}) ) )
    Mousetrap.bind('shift+o', ()=> ( store.dispatch(push("/game")) ) )
    Mousetrap.bind('shift+c', ()=> ( store.dispatch(push("/game/character")) ) )
    Mousetrap.bind('shift+j', ()=> ( store.dispatch(push("/game/journal")) ) )
    Mousetrap.bind('shift+l', ()=> ( store.dispatch(push("/game/location")) ) )

    console.log("Additional jquery muckery took approx. "+Math.round(p()-delay)+"ms")
})
