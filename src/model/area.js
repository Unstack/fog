import defaults from '../constant/model/area'

export default (props) => ( {...defaults, ...props} )
