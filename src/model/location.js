import defaults from '../constant/model/location'

export default (props) => ( {...defaults, ...props} )
