import area from './area'

import {map} from 'ramda'

export default (size) => ( map( area, Array(size).fill(0) ) )
