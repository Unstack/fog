import ACTIVITY from '../constant/action/activity'

import activities from '../constant/activity'

export default {
  start: (activityname) => ({type:ACTIVITY.START, activity:activities[activityname]}),
  pause: () => ({type:ACTIVITY.PAUSE}),
  continue: (activity) => ({type:ACTIVITY.CONTINUE, activity}),
  stop: () => ({type:ACTIVITY.STOP}),
}
