import LOADER from '../constant/action/loader'

export default {
  ready: () => ( {type:LOADER.READY} ),
  toggle: () => ( {type:LOADER.TOGGLE} )
}
