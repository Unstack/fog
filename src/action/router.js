import ROUTER from '../constant/action/router'

export default {
  go: (target, params) => ({type:ROUTER.GO, target, params})
}
