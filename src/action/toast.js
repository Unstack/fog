import TOAST from '../constant/action/toast'

export default {
  create:(text,options) => ({ type:TOAST.CREATE, text, options }),
  opened:(id) => ({ type:TOAST.OPENED, id }),
  closed:(id) => ({ type:TOAST.CLOSED, id }),
  changed:(id) => ({ type:TOAST.CHANGED, id })
}
