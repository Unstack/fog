import GAME from '../constant/action/game'

export default {
  new:   (name,gender) => ({ type:GAME.NEW, name, gender }),
  load:  (gamestate) => ({type:GAME.LOAD, gamestate}),
  save:  () => ({ type:GAME.SAVE }),
  reset: () => ({ type:GAME.RESET })
}
