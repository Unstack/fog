import UI from '../constant/action/ui'

export default {
  toggleDark: () => ({type:UI.TOGGLE_DARK}),
  reset:      () => ({type:UI.RESET}),
  setTheme:   (theme) => ({type:UI.SET_THEME,theme}),
}
