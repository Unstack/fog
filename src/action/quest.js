import QUEST from '../constant/action/quest'

export default {
  load: (savedata) => ({type:QUEST.LOAD,savedata}),
  unlock: (id) => ({type:QUEST.UNLOCK, id}),
  progress: (id) => ({type:QUEST.PROGRESS, id}),
  finish: (id) => ({type:QUEST.FINISH, id}),
  reset: (id) => ({type:QUEST.RESET, id})
}
