import LOCATION from '../constant/action/location'

export default {
  goto:(location) => ( {type:LOCATION.GOTO, location} )
}
