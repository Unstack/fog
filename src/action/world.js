import WORLD from '../constant/action/world'

export default {
  load:     (savedata) =>     ({type:WORLD.LOAD,     savedata}),
  generate: (world)    =>     ({type:WORLD.GENERATE, world})
}
