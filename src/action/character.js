import CHARACTER from '../constant/action/character'

export default {
  load: (savedata) => ({type:CHARACTER.LOAD,savedata}),
  set:{
    name:        (name)           => ({type:CHARACTER.SET.NAME, name}),
    description: (description)    => ({type:CHARACTER.SET.DESCRIPTION, description}),
    gender:      (gender)         => ({type:CHARACTER.SET.GENDER, gender}),
    race:        (race)           => ({type:CHARACTER.SET.RACE, race}),
    background:  (background)     => ({type:CHARACTER.SET.BACKGROUND, background}),
    guild:       (guild)          => ({type:CHARACTER.SET.GUILD, guild}),
    random:      (property,
                  generator,
                  kind,
                  ...args) => ({type:CHARACTER.SET.RANDOM, generator,
                                                           kind,
                                                           property,
                                                           args}),
    property:    (name,value)     => ({type:CHARACTER.SET.PROPERTY, name, value}),
  },
  go:    (area,location) => ({type:CHARACTER.GO, area, location}),
  eat:   (item)     => ({type:CHARACTER.EAT,    item}),
  drink: (item)     => ({type:CHARACTER.DRINK,  item}),
  tick:  ()         => ({type:CHARACTER.TICK})
}
