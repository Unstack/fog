import SKILL from '../constant/action/skill'

export default {
  load: (savedata) => ({type:SKILL.LOAD,savedata}),
  unlock: (id) => ({type:SKILL.GENERATE, id}),
  progress: (id,exp) => ({type:SKILL.PROGRESS, exp})
}
