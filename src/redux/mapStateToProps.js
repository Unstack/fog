import { createSelector } from 'reselect'

import defaultArea     from '../constant/model/area'
import defaultLocation from '../constant/model/location'

const getArea = ({ world, player:{character:{position}}}) => world === null
                      ? defaultArea
                      : world[position[0]]

const getLocation = ({ world,player:{character:{position}}}) => world === null
                          ? defaultLocation
                          : (
                          world[position[0]]
                            .locations[position[1]] === null
                            ? defaultLocation
                            : world[position[0]]
                                .locations[position[1]]
                          )

const derive = createSelector( [getArea,getLocation],
                                (area,location)=>({area,location}) )

const mapStateToProps = ({loader:  ready,
                 game:  {  world,
                           player,
                           intro },
                  router
                  }) => ({
      ready,
      world,
      player,
      router,
      intro,
      derived:{
        ...derive( {world, player })
      }

})

export default mapStateToProps
