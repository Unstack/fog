import { push } from 'connected-react-router'

import { toast } from 'react-toastify'

import { path } from 'ramda'

import * as act from '../action'

export default (dispatch) => ({
  push:(eventOrURI) => (dispatch(push(eventOrURI.target && eventOrURI.target.dataset
                                       ? eventOrURI.target.dataset.to
                                       : eventOrURI)) ),
  toast:(...args) => (toast.apply(this,args)),
  action:(actionpath,...args) => ( dispatch(path( actionpath.split("."),
                                                  act ).apply(this,args) ) )
})
