export default {
  name: "Foraging",
  description: "Search the ground for neat stuff",
  duration: 6000,
  skills:["Foraging"]
}
