import travel  from './travel'
import fish    from './fishing'
import forage  from './forage'

export default {
  travel,
  fish,
  forage
}
