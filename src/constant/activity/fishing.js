export default {
  name: "Fishing",
  description: "Catch da fishy",
  duration: 10000,
  requires:[ "item:type:Fishing Rod" ],
  skills:["fishing"]
}
