export default {
  TOGGLE_DARK:"UI_TOGGLE_DARK",
  SET_THEME:  "UI_SET_THEME",
  RESET:      "UI_RESET"
}
