export default {
  NEW:      "GAME_NEW",
  LOAD:     "GAME_LOAD",
  SAVE:     "GAME_SAVE",
  RESET:    "GAME_RESET"
}
