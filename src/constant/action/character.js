export default {
  LOAD: "CHARACTER_LOAD",
  SET:{
    NAME:        "CHARACTER_SET_NAME",
    DESCRIPTION: "CHARACTER_SET_DESCRIPTION",
    GENDER:      "CHARACTER_SET_GENDER",
    RACE:        "CHARACTER_SET_RACE",
    BACKGROUND:  "CHARACTER_SET_BACKGROUND",
    GUILD:       "CHARACTER_SET_GUILD",
    RANDOM:      "CHARACTER_SET_RANDOM",
    PROPERTY:    "CHARACTER_SET_PROPERTY"
  },
  GO:    "CHARACTER_GO",
  EAT:   "CHARACTER_EAT",
  DRINK: "CHARACTER_DRINK",
  TICK:  "CHARACTER_TICK",
}
