export default {
    name:"Borge",
    description:"A quiet town mostly inhabited by farmers and their family members. Covered in snow for greater part of the year.",
    type:"town",
    provides:["shelter"],
    activities:[],
    buildings:[]
}
