import emma from '../npc/emma'

export default {
    name:"An encampment",
    description:"A small encampment in the middle of a field. There is a single tent in front of a small fire.",
    type:"LOCATION_HOME",
    provides:["shelter","health","mana","energy"],
    npcs:[
      emma
    ]
}
