import {default as borge       } from './borge'
import {default as lake_sirena } from './lake_sirena'
import {default as camp        } from './camp'

import * as set_1 from './set_1'

export default {
  borge,
  lake_sirena,
  set_1,
  camp
}
