export default {
    name:"Lake Sirena",
    type:"lake",
    provides:["water"],
    activities:["fish","forage"],
    buildings:[]
}
