import characterDefaults from '../model/character'

export default {
  ...characterDefaults,
  name: "Emma"
}
