import flask from '../item/flask.js'

export default {
    name:"Lancaster's General Goods",
    type:"store",
    activities:["buy","sell"],
    provides:[ flask ],
}
