import location_lake_sirena from '../location/lake_sirena'

export default {
  name: "Sirena Fields",
  locations:[
    location_lake_sirena
  ]
}
