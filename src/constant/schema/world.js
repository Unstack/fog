import areaSchema from './area'

export default  {
        type      : "array",
        minItems  : 25,
        maxItems  : 25,
        items     : areaSchema
}


