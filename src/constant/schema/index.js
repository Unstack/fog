import world    from './world'
import area     from './area'
import location from './location'

const schemas = [
  world,
  area,
  location
]

schemas.world    = world
schemas.area     = area
schemas.location = location

export default schemas
