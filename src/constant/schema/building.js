export default {
  type: "object",
  properties: {
    name: { faker:  "name.lastName" }
  },
  required: ["name"]
}

