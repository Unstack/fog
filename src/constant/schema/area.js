import locationSchema from './location'

export default {
  type: "object",
  properties: {
    name: { faker:  "name.lastName" },
    fog: {
      chance: {
        integer: {
          min:0,
          max:80
        }
      }
    },
    locations: {
      type: "array",
      minItems:1,
      maxItems:5,
      items: locationSchema
    }
  },
  required: ["fog","name","locations"]
}



