import buildingSchema from './building'

export default {
  type: "object",
  properties: {
    name: { faker:  "name.lastName" },
    size: { chance:  {
      "integer" : {min: 1, max: 9}
    } },
    type: { fog:    {
                      //Weight for randomization
      "location.type": {weights:[3,2,2,2,2,2,1]}
    } },
    provides: {
      fog: {
        "location.provides": "#/provides/fog"
      }
    },
    buildings: {
      type: "array",
      minItems:1,
      maxItems:3,
      items: buildingSchema
    }
  },
  required: ["name","size","type"]
}


