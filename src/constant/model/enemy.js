import attributes from '../list/attributes'

export default {
  name: "Nameless enemy",
  health: {current: 15, max: 15},
  mana: null,
  attributes
}
