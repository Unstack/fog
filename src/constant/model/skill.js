export default {
  name: "Nameless Skill",
  level: 1,
  exp: 0,
  tnl: [10,30,60,90,160,230,410,590,720,940,1040],
  unlocked:false
}
