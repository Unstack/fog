import attributes from '../list/attributes'

export default {
    name:    "Nameless Character",
    gender:   0,
    position: [0,0],
    health:  {current:15, max:15 },
    mana:    {current:5,  max:5  },
    hunger:  {current:100,max:100},
    thirst:  {current:100,max:100},
    effects:   [],
    inventory: [],
    equipment: [],
    attributes
}
