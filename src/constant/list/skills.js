const skill_base = {
  level:0,
  exp:0,
  tnl:[15,25,50,100,150,250,350,600,800,950]
}

//semantic: Skills set to level 1 will be initial skills

const skill_foraging = {
  ...skill_base,
  name:"Foraging",
  description:"Scour the earth in search of useful resources.",
  attributes:['dex','int','con'],
  level:1,
  actions:[
    [1,{name:"Forage the fields",requires:"LOCATION_FIELD", activity:"forage"},
       {name:"Forage the beach" ,requires:"LOCATION_BEACH",  activity:"forage"},
       {name:"Forage around the lake" ,requires:"LOCATION_LAKE", activity:"forage"} ],
    [2,{name:"Forage forest",requires:"LOCATION_FOREST", activity:"forage"} ]

  ]
}

const skill_crafting = {
  ...skill_base,
  name:"Crafting",
  description:"Make basic tools from natural resources.",
  attributes:['wis','str','int']
}

const skill_construction = {
  ...skill_base,
  name:"Construction",
  description:"Allows for the construction and erection of planks and buildings.",
  attributes:['str','int','int']
}

const skill_mining = {
  ...skill_base,
  name:"Mining",
  description:"Mine precious minerals straight from the earth.",
  attributes:['str','con','dex']
}

const skill_woodcutting = {
  ...skill_base,
  name:"Woodcutting",
  description:"Kill trees and chop their remains into logs.",
  attributes:['str','con','cha'],
  level: 1,
  actions:[
   [1,  { name:"Fell a small tree", requires:"LOCATION_FOREST", activity:"woodcut.fell"}
        ],
   [10, { name:"Fell a decent-sized tree",
          requires:"LOCATION_FOREST", activity:"woodcut.fell-medium"}],
   [20, { name:"Fell a big tree",
          requires:"LOCATION_FOREST", activity:"woodcut.fell-big"}],
  ]
}

const skill_fishing = {
  ...skill_base,
  name:"Fishing",
  description:"Kidnap and murder a variety of sea creatures.",
  attributes:['con','wis','cha'],
  level: 1,
  actions:[
    [1,  {name:"Fish near the lake", requires:"LOCATION_LAKE",   activity:"fish"},
         {name:"Fish at the beach",  requires:"LOCATION_BEACH",  activity:"fish"} ],
    [10, {name:"Fish on the ocean",  requires:"LOCATION_OCEAN",  activity:"fish"} ],
    [20, {name:"Cast fishing net",   requires:"LOCATION_LAKE",   activity:"fish"}   ]
    ]
}

const skill_meditation = {
  ...skill_base,
  name:"Meditation",
  description:"Ability to keep the mind and body at peace.",
  attributes:['wis','con','dex'],
  level: 1,
  actions: [
        [1, { name:"Sleep (Restore HP)",
              requires:"LOCATION_HOME",
              activity:"sleep" },
            { name:"Meditate (Restore MP)",
              requires:"LOCATION_HOME",
              activity:"sleep.meditate"} ]
   ]
}

const skill_bartering = {
  ...skill_base,
  name:"Bartering",
  description:"Heckle the person you're selling to for a better deal.",
  level:1,
  attributes:['cha','int','wis'],
  actions:[
      [1,  {name:"Sell something",requires:"LOCATION_TOWN",  activity:"barter.sell"},
           {name:"Buy something" ,requires:"LOCATION_TOWN",  activity:"barter.buy"} ]
  ]
}

const skill_farming = {
  ...skill_base,
  name:"Farming",
  description:"Plants and harvest a variety of crops straight from the earth.",
  attributes:['wis','str','dex'],
}

const skill_cooking = {
  ...skill_base,
  name:"Cooking",
  description:"Create mouth-watering delicacies from various ingredients.",
  attributes:['dex','int','con'],
}

const skill_brewing = {
  ...skill_base,
  name:"Brewing",
  description:"Plants and harvest a variety of crops straight from the earth.",
  attributes:['con','cha','str'],
}

const skill_alchemy = {
  ...skill_base,
  name:"Alchemy",
  description:"Enchant regular items into artifacts by dipping them in rare brews.",
  attributes:['int','wis','dex'],
}

const skill_smithing = {
  ...skill_base,
  name:"Smithing",
  description:"Smelt ores into bars of metal and turn them into various useful goods.",
  attributes:['str','con','int'],
}

export default [
  skill_foraging,
  skill_woodcutting,
  skill_crafting,
  skill_construction,
  skill_mining,
  skill_fishing,
  skill_meditation,
  skill_bartering,
  skill_farming,
  skill_cooking,
  skill_brewing,
  skill_alchemy,
  skill_smithing
]
