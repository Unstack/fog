export default [
  {
    name:'Strength',
    short: 'str',
    value:4
  },
  {
    name:'Constitution',
    short: 'con',
    value:4
  },
  {
    name:'Wisdom',
    short: 'wis',
    value:4
  },
  {
    name:'Intelligence',
    short: 'int',
    value:4
  },
  {
    name:'Dexterity',
    short:'dex',
    value:4
  },
  {
    name:'Charisma',
    short:'cha',
    value:4
  }
]
