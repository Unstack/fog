import allSkills from '../list/skills'

const skills = allSkills.filter((skill)=> skill.level > 0 )

export default skills
