import tutorials from './tutorials'

const quests = [
  ...tutorials,
]

export default quests
