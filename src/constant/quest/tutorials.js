const tutorial_1 = {
  name: "Tutorial 1",
  parts: [
    "Emma wants you to go fetch some driftwood off the beach, sounds simple enough. If we go down to the beach we should find some in no time.",
    "Alright we got to the beach, now we should try and forage here to see if we can find any driftwood laying around, I'm pretty sure the ocean won't mind us taking some.",
    "There we go, that wasn't hard to find at all. Let's return to Emma and see what she was planning to do with this."
  ],
  goals: [
    ["arrive:Sirena Beach","QUEST.PROGRESS",  "Tutorial_1" ],
    ["acquire:Driftwood",  "QUEST.PROGRESS",  "Tutorial_1" ],
    ["talk:Borge:Emma",    "QUEST.FINISH",    "Tutorial_1" ]
  ],
  reward:[
    null,null,
    ["character.give:Simple Knife"],
  ],
  progress:0
}

const tutorial_2 = {
  name: "Tutorial 2",
  parts: ["Emma taught you the basics of crafting and how to cut a rudamentary handle from a piece of wood. She asked you to try making one yourself by cutting the Driftwood you've found into a handle. She gave you a knive as well, sweet.",
  "Well that wasn't so hard, and without cutting yourself either. Now let's show what we've made to Emma."],
  goals: [
    ["acquire:Driftwood Handle", "QUEST.PROGRESS",  "Tutorial_2"],
    ["talk:Borge:Emma",          "QUEST.FINISH",    "Tutorial_2"]
  ],
  rewards: [
    ["skill.increase:foraging,50"]
  ],
  progress:0
}

const tutorial_3 = {
  name: "Tutorial 3",
  parts: ["IDK"],
  goals: [],
  progress: 0
}

const tutorials = [
  tutorial_1,
  tutorial_2,
  tutorial_3
]

export default tutorials
