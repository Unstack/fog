
const story_1 = {
  name: "The fog that came",
  parts: ["You found a dusty book written in a strange language you've never even see before. You should show this to Emma and see if she knows more.",
          "Emma says she has no clue what the book is on about, but there's a librarian in Dante that could possibly know more. We should go over and visit to see if he knows anything about the book.",
        "We've met the librarian in Dante named Ollie. He seemed to know a little Xarxean, the rather ancient language that the book apparantly is written in. It'll take him a few days but he'll definitely be able to tell us all about the book after that."
      "It's been a day since we gave that Xarxean book to Ollie to study. We should probably check back in and see how he's doing."
      ]
  goals: [
    ["talk:Borge:Emma",  "QUEST.PROGRESS", "story_1"],
    ["talk:Borge:Emma",  "QUEST.PROGRESS", "story_1"],
    ["talk:Dante:Ollie", "QUEST.PROGRESS", "story_1"],
    ["time:hour:24",     "QUEST.PROGRESS", "story_1"],
    ["talk:Dante:Ollie", "QUEST.PROGRESS", "story_1"],
  ]
}

export [
  story_1
]
'
