import ROUTER       from '../constant/action/router'
import defaultState from '../constant/reducer/router'

const RouterReducer = (state = defaultState,
                         action) => {
  switch (action.type){
    case ROUTER.GO: {
      const {target="/404",params} = action;
      return {target, params}
    }
    default:{
      return state
    }
  }
}

export default RouterReducer
