import {combineReducers} from 'redux'
import { connectRouter } from 'connected-react-router'

import history from '../util/history'

import loader from './loader'

import { default as world      }  from './world'
import { default as player     }  from './player/'
import {default as intro       }  from './intro'

export default combineReducers({
    loader,
    router: connectRouter(history),
    game: combineReducers({world, player,intro})
})

