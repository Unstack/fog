const introReducer = (state = false,
                         action) => {
  switch (action.type){
    case "INTRO":
      return !state
    default:
      return state
  }
}

export default introReducer
