import LOADER       from '../constant/action/loader'
import initialState from '../constant/reducer/loader'

const loaderReducer = (state=initialState, action) => {
  switch (action.type){
    case LOADER.READY: {
      return true
    }
    case LOADER.TOGGLE: {
      return state ? false : true
    }
    default:{
      return state
    }
  }
}

export default loaderReducer
