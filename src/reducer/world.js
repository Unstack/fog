import WORLD        from '../constant/action/world'
import defaultState from '../constant/reducer/world'

import {merge} from 'ramda'

const worldReducer = (state = defaultState,
                         action) => {
  switch (action.type){
    case WORLD.LOAD: {
      if (!action.savedata)
        console.warn("WORLD.LOAD was called without passing a world, using default fallback")
      return action.savedata || defaultState
    }

    case WORLD.FIND: {
      return merge(state)
    }
    default:{
      return state
    }
  }
}

export default worldReducer
