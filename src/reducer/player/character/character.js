import {merge} from 'ramda'

import CHARACTER    from '../../constant/action/character'
import defaultState from '../../constant/reducer/character'

const characterReducer = (state = defaultState,
                          action) => {
  switch (action.type){
    case CHARACTER.LOAD:
      return merge(state,action.savedata)

    case CHARACTER.SET.NAME:
      return merge(state,{name:action.name})

    case CHARACTER.SET.BACKGROUND:
      return merge(state,{background:action.background})

    case CHARACTER.SET.GENDER:
      return merge(state,{gender:parseInt(action.gender,10)})

    case CHARACTER.SET.RACE:
      return merge(state,{race:action.race})

    case CHARACTER.SET.GUILD:
      return merge(state,{background:action.guild})

    case CHARACTER.SET.PROPERTY: {
      const change = {}
            change[action.name] = action.value
      return merge(state, change)
    }

    case CHARACTER.EAT: {
      const current = state.hunger.current - action.nutrition < 0
                      ? 0
                      : state.hunger.current - action.nutrition,
             hunger = {...state.hunger, current}
      return merge(state,{hunger})
    }

    case CHARACTER.DRINK: {
       const current = state.thirst.current - action.nutrition < 0
                       ? 0
                       : state.thirst.current - action.nutrition,
             thirst = {...state.thirst, current}
      return merge(state,{thirst})
    }

    case CHARACTER.GO: {
      const position = [Number.isInteger(action.area)?action.area : state.position[0],
                        Number.isInteger(action.location) ? action.location : state.position[1]]
      return merge(state,{position})
    }
    default:{
      return state
    }
  }
}

export default characterReducer
