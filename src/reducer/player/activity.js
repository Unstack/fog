import ACTIVITY from '../../constant/action/activity'
import defaultState from '../../constant/reducer/activity'

import {merge} from 'ramda'

const ActivityReducer = (state = defaultState,
                         action) => {
  switch (action.type){
    case ACTIVITY.START:

      return merge(state,action.activity,{started:Date.now()})
    case ACTIVITY.STOP:
      return null
    default:{
      return state
    }
  }
}

export default ActivityReducer
