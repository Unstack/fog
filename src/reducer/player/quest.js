import {merge,insert,remove} from 'ramda'

import QUEST        from '../../constant/action/quest'
import defaultState from '../../constant/reducer/quest'

const questReducer = (state = defaultState,
                         action) => {
  switch (action.type){
    case QUEST.PROGRESS: {
      const quest = state.quests[action.questid],
            modifiedQuest = merge(quest, {progress:quest.progress+1} ),
            withoutQuest = remove(action.questid,1,state.quests)

      return merge(state,{quests: insert( action.questid,
                                          modifiedQuest,
                                          withoutQuest) })
    }
    default:{
      return state
    }
  }
}

export default questReducer
