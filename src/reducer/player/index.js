import character from './character'
import quest     from './quest'
import skill     from './skill'
import activity  from './activity'

import {combineReducers} from 'redux'

const playerReducer = combineReducers({
  character,
  quest,
  skill,
  activity
})

export default playerReducer
