import defaultState from '../constant/reducer/location'
import LOCATION     from '../constant/action/location'

const locationReducer = (state=defaultState, action) => {
  switch (action.type){
    case LOCATION.GOTO:{
     return action.location
    }
    default:{
      return state
    }
  }
}

export default locationReducer
