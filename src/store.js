import {createStore,applyMiddleware, compose} from 'redux'

import { routerMiddleware } from 'connected-react-router'

import createSagaMiddleware from 'redux-saga'

import initialState  from './redux/initialState'

import rootReducer   from './reducer'
import rootSaga      from './saga'

import history from './util/history'

const sagaMiddleware  =  createSagaMiddleware()

const store  = createStore( rootReducer,
                            initialState,
                            compose(
                             applyMiddleware(routerMiddleware(history),
                                             sagaMiddleware),
                            window.__REDUX_DEVTOOLS_EXTENSION__ &&
                            window.__REDUX_DEVTOOLS_EXTENSION__()
                            ))

sagaMiddleware.run(rootSaga)

export default store
