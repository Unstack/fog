import React                from 'react'
import ReactDOM             from 'react-dom'
import {Provider, connect}  from 'react-redux'
import { ConnectedRouter }  from 'connected-react-router'

import gameWorker           from './worker/game.worker'

import store                from './store'

import Page                 from './component/Page'

import mapStateToProps      from './redux/mapStateToProps'
import mapDispatchToProps   from './redux/mapDispatchToProps'

import * as serviceWorker   from './util/serviceWorker'
import history              from './util/history'

import './style'

window.store = store
window.dpatch = (type,args) => ( store.dispatch({type,...args}) )
window.gworker = new gameWorker()

window.gworker.onmessage = (e) => (console.log(e))

window.gworker.postMessage("HOI")

const ReduxPage = connect( mapStateToProps,mapDispatchToProps )( Page )

const WrappedReduxPage = ({store}) =>
                            (<Provider store={store}>
                              <ConnectedRouter history={history}>
                                < ReduxPage />
                              </ConnectedRouter>
                            </Provider>)

WrappedReduxPage.defaultProps = {store}

const rootElement = document.getElementById('root');

ReactDOM.render(< WrappedReduxPage />, rootElement );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
