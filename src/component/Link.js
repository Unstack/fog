import { createLink } from 'redux-saga-router/react'

import history from '../util/history'

const Link = createLink(history)

export default Link
