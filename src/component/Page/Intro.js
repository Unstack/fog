import React from 'react'

import Container from '../Container'
import Card      from "../Card"

import defaultCharacter from '../../constant/model/player'

const IntroCard = ({character:{name},returning,error}) =>
            (<Container>
                <Card title="The fog" className="animated fadeIn slow text-center" flat>
                  <p>
                    Intro placeholder.
                  </p>
                </Card>
              </Container>)

IntroCard.defaultProps = {
  character: defaultCharacter,
  returning:true,
  error:null
}

export default IntroCard
