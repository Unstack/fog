import React from 'react'

import IntroCard from './Intro'

import Container from '../Container'
import Card      from "../Card"

import defaultCharacter from '../../constant/model/player'

import iconLoader from '../../icon/reload.svg'

const ErrorCard = ({error}) => (
  <Card title={error.name+" Error"} className="animated fadeIn fast" flat>
    <p> You broke something, congratulations - Have a cookie.</p>
    <code style={{whiteSpace:"pre"}}>{JSON.stringify(error,null,2)}</code>
  </Card>
)

const LoaderCard = () => (
  <Card title="Loading" className="animated fadeIn fast text-center" flat>
                  <p className="animated fadeIn slower delay-1s">
                    Please hold tight while we prepare some things for you
                  </p>
                  <div className="p-2 m-1 animated fadeIn slower delay-2s">
                    <img src={iconLoader}
                         className="spin"
                         alt="spinner icon"
                         style={{ width:"10vw", height:"auto"}}/>
                  </div>
                  <p id="currentlyLoading" className="animated fadeIn slower delay-3s">
                    ...
                  </p>
                </Card>
)

const ContainerCard = ({error,intro}) =>(
  <Container className="animated fadeInUp">
    {  error
      ?   <ErrorCard error={error} />
      :   intro
              ? <IntroCard/>
              : <LoaderCard/>
    }
  </Container>
)

LoaderCard.defaultProps = {
  character: defaultCharacter,
  intro:false,
  error:null
}

export default ContainerCard
