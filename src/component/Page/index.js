import React,{Component} from 'react'

import Game       from '../Game'

import Layout     from './Layout'
import Loader     from './Loader'

class App extends Component {

  constructor(props){
      super(props)
      this.state = {
        error:false
      }
  }

  static getDerivedStateFromError(error) {
    return { error }
  }

  render() {
    return  <Layout {...this.props} >
                { this.props.ready === false || this.state.error
                   ? <Loader error={this.state.error}
                             character={this.props.player}
                             intro={this.props.intro} />
                   : <Game {...this.props} /> }
            </Layout>

  }
}
export default App
