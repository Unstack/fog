import React from 'react'

import Container from '../Container'

import wrenchIcon from '../../icon/wrench.svg'

const Footer = ({className,id}) => (
 <Container className={"text-right "+className} id={id}>
    <img src={wrenchIcon} className="bg-light m-1 p-1 mr-2 borders rounded-circle"
                          alt="wrench icon"
                          style={{width:"24px",height:"auto"}} />
    <a href="http://gitlab.com/unstack"
      target="_blank"
      rel="noopener noreferrer"
      className="animated text-light fadeIn delay-1s">
      Unstack
    </a>
  </Container>
)

export default Footer
