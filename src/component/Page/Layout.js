import React       from 'react'
import { Switch,Route }  from 'react-router'

import PageNav      from './Nav'
import Footer       from './Footer'

import Container    from '../Container'
import GameNav      from '../Game/Nav'

const Page = (props) =>
  (<Container fluid className="p-0 m-0" id="content">

        <Switch>
          <Route exact path="/(home|about|settings|debug)"
                render={()=> <PageNav {...props}/> } />
          <Route path = "/game"
                render={()=> <GameNav {...props} /> } />
        </Switch>

        <Switch>
          {props.children}
        </Switch>

      <Footer className=" bg-primary mb-4 rounded-0 animated fadeIn delay-1s" id="footer" />
  </Container>)

export default Page
