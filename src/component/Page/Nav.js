import React from 'react'

import Link from '../Link'

import about from '../../constant/about'

import menuIcon from '../../icon/menu.svg'

import logo from '../../logo.svg'

const PageNav = ({push,className}) => (
  <nav className={"navbar navbar-light navbar-expand-lg bg-primary mb-2 "+
                  className} id="navbar">
    <Link to="/home">
      <img src={logo}
               alt="logo"
               className="navbar-brand animated bounceInLeft delay-1s"
               style={{width:"25%",height:"auto"}}/>
    </Link>

      <h1 className="h2 p-0 m-0 navbar-text">
        <Link to={"/game"}>{about.name}</Link>
      </h1>

    <button className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation">
      <img src={menuIcon}
           className="pb-1"
           alt="menu icon"
           style={{width:"inherit",height:"auto"}}/>
      <span className="d-none">Menu</span>
    </button>

     <div className="collapse navbar-collapse" id="navbarNav">
      <div className="navbar-nav">
          <Link to="/home" className="nav-item nav-link">Home</Link>
          <Link to="/about" className="nav-item nav-link">About</Link>
          <Link to="/debug" className="nav-item nav-link">Debug</Link>
          <Link to="/settings" className="nav-item nav-link">Settings</Link>
      </div>
    </div>
  </nav>
)

export default PageNav
