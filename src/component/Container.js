import React from 'react'

const Container = ({children, id, className, fluid,style}) => (
  <div className={(fluid?"container-fluid ":"container ")+(className?className:"")}
      id={id}
      style={style}
  >
    {children}
  </div>
)

Container.Row = ({children, id, className}) => (
  <div className={"row "+(className?className:"")} id={id} >
    {children}
  </div>
)


Container.Column = ({children, id, className}) => (
  <div className={"col "+(className?className:"")} id={id} >
    {children}
  </div>
)



export default Container
