import React,{Fragment} from 'react'

import Container from './Container'

const Card = ({ title,text,
                children,className,
                underline,showTitle,headerSize}) => (
  <Container className={ "border border-primary bg-theme-2 "+
                         "pt-3 pt-md-0 pb-3 rounded-top "+className} >
    {children && !title
    ? children
    : <Fragment>
          {title &&  <Container.Row>
                      <Container.Column>
                      <h1 className={showTitle
                                ? headerSize
                                : headerSize+" d-none text-center d-md-block m-2"}>
                                {title}
                      </h1>
                      </Container.Column>
                     </Container.Row>}

          {title && underline && <hr className="mt-0 d-none d-md-block"/>}
          {children
            ? children
            : <Container.Row>
                <Container.Column className="bg-warning">
                  <p> {text} </p>
                </Container.Column>
              </Container.Row>}
      </Fragment>}
  </Container>
)

Card.defaultProps = {
  underline:true,
  showTitle:false,
  headerSize:"h2",
  title:null,
  text:null,
}
export default Card
