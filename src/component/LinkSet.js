import React from 'react'

import Link from './Link'

import mapIndexed from '../util/mapIndexed'

const ButtonSet = ({set,fallbackText,ariaLabel,vertical,action,className,linkStyle}) => (
   set && set.length > 0
    ? (<div className={(vertical?"btn-group-vertical ":className||"btn-group")}
            role="group"
            aria-label={ariaLabel}>
          {mapIndexed(({to,label,args=[]},key)=>
            (action
              ? <button className={linkStyle||"btn"}
                        onClick={ ()=>( action.apply(this,[to,...args]) ) }
                        key={key}> {label} </button>
              : <Link to={to} className={linkStyle||"nav-link"} key={key}>{label}</Link>),
            set)}
      </div>)
    : (<p className="font-italic"> {fallbackText} </p>)
)

ButtonSet.defaultProps = {
  set:[],
  fallbackText: "No items in set",
  ariaLabel:"Button set",
  vertical:false,
  action:null
}

export default ButtonSet
