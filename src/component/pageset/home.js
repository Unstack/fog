import React from 'react'

import Card      from '../Card'
import Container from '../Container'

import randomizeIcon from '../../icon/reload.svg'

const NewCharacterForm = ({player:{character:{name,gender}},action,toast}) => (
      <form className="needs-validation bg-light p-4" noValidate>
        <div className="input-group mb-3">
          <input type="text"
               name="name"
               id="playername"
               className="form-control"
               data-tip="Alphanumerical only, 3 - 24 characters"
               value={name}
               onChange={( {target:{value:name}})=> action("character.set.name",name) }
               placeholder="Character Name" required />
          <div className="invalid-feedback">
            Alphanumerical only, 3 - 24 characters
          </div>
        </div>

        <div className="btn-group-toggle mb-3" data-toggle="buttons">
          <label className={"btn btn-secondary "+(gender?"active":"")}
                 onClick={ ()=> action("character.set.gender", 0) }>
            <input className="form-check-input"
                  type="radio"
                  name="gender"
                  id="genderRadio1"
                  autoComplete="off"
                  value="male"
                  defaultChecked />
                  Male
          </label>
          <label className={"btn btn-secondary "+(!gender?"active":"")}
                 onClick={ ()=> action("character.set.gender", 1) }>
            <input className="form-check-input"
                  type="radio"
                  name="gender"
                  id="genderRadio2"
                  autoComplete="off"
                  value="female" />
                  Female
          </label>
          <label className="btn btn-secondary"
                 onClick={ ()=> action("character.set.gender", -1) }>
            <input className="form-check-input"
                  type="radio"
                  name="gender"
                  id="genderRadio3"
                  autoComplete="off"
                  value="problematic" />
                  It's complicated
          </label>
        </div>

        <div className="btn-group col p-0">
          <input type="button" className="btn"
                 onClick={(e)=>{ action("character.set.random","name",
                                                               "faker",
                                                               "name.findName") &&
                                action("character.set.random","gender",
                                                               "chance",
                                                               "integer",
                                                               {min:0,max:1}) &&
                                 e.preventDefault() } }
                 value="RANDOMIZE" />
          <input type="button" className="btn btn-primary btn-block"
                 onClick={(e)=>{action("game.new") && e.preventDefault() } }
                 value="PLAY NOW" />
        </div>
      </form>)

NewCharacterForm.defaultProps = {
  loginErr:null
}

const HomePage = ({world,player,action,push}) => (
  <Card title="Home">
    <p>
      Fog is a post-apocalyptic survival game in early Y2K settings that you can play straight from your browser, no downloads required. Explore a randomly generated world filled to the brink with fascinatingly locations, deep dark dungeons, mundane town folk and plenty of immersive gameplay features for you to enjoy. Play on whatever device you want as well - be it on mobile or on the browser. You never have to be without the fog.
    </p>
    {world===null
      ? <NewCharacterForm action={action} player={player} />
      : <div className="form-group text-center">
          <input type="submit" className="btn btn-primary btn-lg shadow
                                        animated pulse slower infinite"
                onClick={ (e)=>{ push("/game") } }
                value="RETURN TO THE FOG"
                />
        </div>
      }

      <div className="bg-light text-center animated slideInRight">
      <p className="m-2">
        Start playing today and we promise not to implement loot boxes for another year!
      </p>
      <p className="text-right m-2">- Broke developer</p>
      </div>

 </Card>
)

export default HomePage
