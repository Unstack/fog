import React from 'react'

import Card from '../Card'

const Error404Page = (props) => (

    <Card title     = "404"
          text      = "This page doesn't exist you fool."
          className = "text-center animated shake"/>

)

export default Error404Page


