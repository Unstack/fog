import {default as home}     from './home'
import {default as about}    from './about'
import {default as settings}    from './settings'
import {default as debug}    from './debug'
import {default as error404} from './404'

import {default as game}     from './game'


export default [
  {path:"/home",     component:home},
  {path:"/debug",    component:debug},
  {path:"/404",      component:error404},
  {path:"/about",    component:about},
  {path:"/settings", component:settings},
  ...game
]
