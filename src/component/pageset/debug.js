import React from 'react'

import Card from '../Card'

const data = []

const DebugPage = () => (

    <Card title     = "Debug"
          text      = {<code id="debug"
                        style={{whiteSpace:'pre'}}>{JSON.stringify(data,null,2)}</code>}
          flat   />

)

export default DebugPage


