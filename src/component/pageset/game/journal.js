import React from 'react'

import Card      from '../../Card'

import Journal from '../../Game/Journal'

import defaultQuest from '../../../constant/quest'


const JournalPage = ({player:{quest=defaultQuest},
                      match:{params:{page=0}} }) => (

    <Card title     = "Journal"
          className = "animated fadeInUp" >
        <Journal quest={quest} page={page} />
   </Card>

)

export default JournalPage


