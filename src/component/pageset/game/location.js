import React from 'react'

import Card from '../../Card'

import Location from '../../Game/Location'

const LocationPage = ({ player,
                        action,
                        derived:{area},
                        match:{params:{locationid=player.character.position[1],
                                        part,
                                        id}}}) => (

    <Card title     = "Location"
          className = "animated fadeInUp" >
      <Location location={ area.locations[parseInt(locationid,10)] }
                locationId = { parseInt(locationid,10) }
                player={ player }
                part={ part }
                partId= { parseInt(id,10) }
                area={area}
                action={ action } />
   </Card>

)

export default LocationPage


