import React,{Fragment} from 'react'

import ReactTooltip from "react-tooltip"

import Location from '../../Location'
import LocationTile from '../../Location/Tile'

import Map from '../../Game/Map'

import mapIndexed from '../../../util/mapIndexed'

import Card      from '../../Card'

const GameWorldPage = ({ world,push,action,
                         player:{character:{position:[currentareaid,currentlocationid]}},
                         player,
                         match:{params:{areaid:areaidstring,
                                        locationid:locationidstring}}}) =>
    {
      const areaid = parseInt(areaidstring,10)
      const locationid = parseInt(locationidstring,10)

      return (<Card title="World" className="animated fadeInUp" >
          <ReactTooltip multiline={true} />
          {Number.isInteger(areaid)
            && world[areaid]
            ? (
              Number.isInteger(locationid)
              && world[areaid].locations[locationid]
              ? <div>
                  <Location
                    area={world[areaid]}
                    areaid={areaid}
                    location={ world[areaid].locations[locationid] }
                    locationid = { locationid }
                    player={ player }
                    action={ action } />
                    <button className="btn btn-info btn-block btn-lg mt-3"
                        onClick={()=> push("/game/world/"+areaid) }>
                        Back
                    </button>
                </div>
              : <Fragment>
                  <div className="row mb-3">
                  {mapIndexed((location,sublocationid)=>(
                     <LocationTile
                        area={world[areaid]}
                        location={ location }
                        areaid={areaid}
                        locationid = { sublocationid }
                        push={ push }
                        key={sublocationid}
                        current={ currentareaid===areaid &&
                                  currentlocationid === sublocationid }
                        className="col-12 col-md-6 col-lg-4 col-xl-2 rounded" />),
                        world[areaid].locations )}
                  </div>
                  <button className="btn btn-info btn-block btn-lg mt-3"
                          onClick={()=> push("/game/world") }>
                    World Map
                  </button>
                </Fragment>
              )
               : <div id="map">
                  <Map world={world}
                        areaid={currentareaid}
                        viewid={areaid}
                        push={push} />
                </div> }

      </Card>)
}
export default GameWorldPage


