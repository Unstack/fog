import React from 'react'

import Container from '../../Container'
import Card from '../../Card'


const LoadoutPage = ({player:{activity},derived:{location}}) => (

    <Card title     = "Activity"
          className = "animated fadeInUp">
        <Container>
          {!activity && <p>You're not really doing anything.</p>}
          {activity && <p> You're currently {activity.name.toLowerCase()}
                            <span> at </span>{location.name}.</p>}
        </Container>
   </Card>

)

export default LoadoutPage


