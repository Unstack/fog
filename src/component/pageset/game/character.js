import React from 'react'

import Card from '../../Card'

import CharacterInfo from '../../character/Info'

const LoadoutPage = ({player}) => (

    <Card title     = "Character"
          className = "animated fadeInUp" >
        < CharacterInfo {...player}
                        headerSize="h3" />
   </Card>

)

export default LoadoutPage


