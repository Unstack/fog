import {default as world}      from './world'
import {default as character}  from './character'
import {default as overview}   from './overview'
import {default as activity}   from './activity'

export default [
  {path:'/game/world/:areaid?/:locationid?', component:world},
  {path:'/game/character/:item?',            component:character},
  {path:'/game/activity/:action?',           component:activity},
  {path:'/game/:page?',                      component:overview },
]
