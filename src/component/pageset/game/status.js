import React from 'react'

import Card from '../../Card'

import CharacterInfo from '../../character/Info'

const GameStatusPage = ({player}) => (

    <Card title     = "Status"
          className = "animated fadeIn"
          flat>
          < CharacterInfo {...player}
          headerSize="h4" />
   </Card>

)

export default GameStatusPage


