import React from 'react'

import Card from '../../Card'

import Journal         from '../../Game/Journal'

import defaultQuest from '../../../constant/quest'

const OverviewPage = ({player,match:{params:{page=0}}}) => (

    <Card title     = "Overview"
          className = "animated fadeInUp" >
          < Journal quest={player.quest||defaultQuest} page={page} />
   </Card>

)

export default OverviewPage


