import React from 'react'

import Card from '../Card'


const SettingsPage = ({action}) => (
    <Card title="Settings" flat>
        <div className="form-group row justify-content-around">
          <div className="col-5">
          <label>Reset everything</label>
          </div>
          <div className="col-5 p-0">
            <button
              className="btn btn-danger"
              data-tip="WARNING: There's no going back from this
                        <br/> Your progress WILL be lost"
              onClick={()=>( action("game.reset") )}
              >
              Reset
            </button>
          </div>
        </div>
    </Card>
)

export default SettingsPage


