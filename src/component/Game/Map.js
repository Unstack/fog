import React from 'react'

import mapIndexed from '../../util/mapIndexed'

const Map = ({world,areaid,viewid,push}) => {
  const world_len_sqrt = Math.sqrt(world.length)

  return (
          <svg  viewBox="0 0 100 100"
                style={{font:"12px sans-serif"}}>

          {
            mapIndexed( (area,key)=>(
              <g key={key}
              onClick={ ()=>(push("/game/world/"+key)) }
              className="maptile"
              data-tip={area.name+"<br/> "+
                        (key!==areaid
                        ?"It would take you "+
                          Math.abs(key-areaid)+
                        " hour(s) to travel here"
                        : "You are currently here")+
                        (area.fog>75
                          ?"<br/>Warning: Hazardous levels of fog, prolonged exposure will harm your health"
                          :"")
                        }>
                <rect x={ Math.floor(key%world_len_sqrt) *
                          Math.floor(100/world_len_sqrt)  }
                      y={ Math.floor(key/world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) }
                      width={100/world_len_sqrt}
                      height={100/world_len_sqrt}
                      fill={area.fog>=25?(area.fog>=50?"darkgreen":"green"):"gray"}
                      strokeWidth={1} />
              { true ?
               <g>
               <text x={ Math.floor(key%world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 1.5}
                    y={ Math.floor(key/world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 5}
                    style={{fontSize:"0.25em"}}>
                          {area.name}
              </text>

              <text x={ Math.floor(key%world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 7.5}
                    y={ Math.floor(key/world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 11}
                    style={{fontSize:"0.20em"}}>
                          {area.fog}%
              </text>
              { areaid===key &&
              <text x={ Math.floor(key%world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 2.5}
                    y={ Math.floor(key/world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 15}
                    style={{fontSize:"0.2em",fill:"lightgray"}}>
                    You are here
              </text> }
              </g>
              : <text x={ Math.floor(key%world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 6.5}
                    y={ Math.floor(key/world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 14}
                    style={{fontSize:"1em"}}>
                          ?
              </text>
              }
              { viewid===key &&
              <text x={ Math.floor(key%world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 5}
                    y={ Math.floor(key/world_len_sqrt) *
                          Math.floor(100/world_len_sqrt) + 18}
                    style={{fontSize:"0.2em",fill:"white"}}>
                    Selected
              </text> }

            </g>
            ), world)
          }
          </svg>)
}

export default Map
