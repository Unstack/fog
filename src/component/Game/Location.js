import React,{Fragment} from 'react'

import {equals} from 'ramda'

import LinkSet   from '../LinkSet'
import Container from '../Container'

import LocationBuildings from '../Location/Buildings'
import LocationNpcs      from '../Location/Npcs'
import LocationActions   from '../Location/Actions'
import NearbyLocations   from '../Location/Nearby'


import defaultLocation from '../../constant/model/location'
import defaultPlayer   from '../../constant/model/player'

import mapIndexed from '../../util/mapIndexed'

/*
const Building = ({location,building,buildingId,player,action}) => (
  <Container fluid>
    <Container.Row>
      <Container.Column>
        <h1 className="h3">
          {building.name} Building
        </h1>
        <p>
          { building.description || "This building is missing a description" }
        </p>
      </Container.Column>
    </Container.Row>
  </Container>
)

const TownHall = ({location,player,action}) => (
  <Container fluid className="animated fadeInTop">
    <Container.Row>
      <Container.Column>
        <h1 className="h3">
          Town Hall
        </h1>
        <p>
          Description goes here.
        </p>
      </Container.Column>
    </Container.Row>
  </Container>
)

const defaultBuildings = {
  hall:TownHall
}*/



const CurrentLocation = ({ area, areaid,
                           location,locationid,
                           player, action}) => (
  <Fragment>
    <LocationBuildings buildings={location.buildings} locationId={locationid} />
    <LocationNpcs npcs={location.npcs} locationId={locationid} />
    <LocationActions location={location}
                     locationId={locationid}
                     player={player}
                     action={action}
                     className="col"/>
   <NearbyLocations location={location} area={area} areaid={areaid} />
  </Fragment>
)

const locationColors = {
  "LOCATION_BEACH":"bg-color-blue",
  "LOCATION_FIELD":"bg-color-green",
  "LOCATION_TOWN":"bg-color-purple",
  "LOCATION_DUNGEON":"bg-dark",
  "LOCATION_LAKE":"bg-info",
  "LOCATION_FOREST":"bg-color-teal",
  "LOCATION_CAMP":"bg-color-indigo",
  "LOCATION_HOME":"bg-color-pink"
}


const OtherLocation = ({area,
                   location,
                   player,
                   areaid,
                   locationid,
                   part,partid,
                   action,
                   className}) => (
    <Container fluid className={className}>
        <Container.Row>
          <Container.Column className={locationColors[location.type]}>
            <p className="text-light text-center mt-4 p-4">
              {location.type.slice(location.type.indexOf("_")+1)}
            </p>
          </Container.Column>
          <Container.Column>
            <h1 className="h3">{location.name}</h1>
            <p>{location.description||"This location is missing a description"}</p>
          </Container.Column>
        </Container.Row>

        <Container.Row>
          <div className="btn-group btn-group-block" role="group">
            <button className="btn btn-primary"
                    onClick={ ()=>( action("character.go", areaid,locationid )) } >
                  View
            </button>
            <button className="btn btn-primary"
                  onClick={ ()=>( action("character.go", areaid,locationid )) } >
                Travel
            </button>
          </div>
        </Container.Row>

    </Container>
)

Location.defaultProps = {
  location: defaultLocation,
  player: defaultPlayer
}
export default Location
