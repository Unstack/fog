import React from 'react'

import {slice} from 'ramda'

import Container from '../Container'
import LinkSet   from '../LinkSet'

import mapIndexed from '../../util/mapIndexed'

const Journal = ({quest,page}) => (
  <Container>
    <Container.Row>
      <Container.Column>
        <LinkSet set={mapIndexed((quest,id)=>(
          {to:"/game/journal/"+id, label:quest.name}
        ),quest)} vertical />
      </Container.Column>

      <Container.Column>
        <h1 className="h3">{quest[parseInt(page,10)].name}</h1>
        {mapIndexed((questpart,key)=>(
          <p key={key}> {questpart} </p>
        ), slice(0, quest[parseInt(page,10)].progress+1,
                    quest[parseInt(page,10)].parts ) )}
      </Container.Column>

    </Container.Row>
  </Container>
)

export default Journal
