import React from 'react'
import Link  from '../Link'

const Breadcrumb = ({push,pathname,className}) =>
(<nav aria-label="breadcrumb " className={className}>
  <ol className="breadcrumb m-0 rounded-0">
    {pathname==="/"
    ?<li className="breadcrumb-item"
          aria-current="page">
          Home
      </li>
    :pathname
         .slice(1)
         .split("/")
        .reduce((col,el)=>[...col,col[col.length-1]+"/"+el ], [""])
        .map((path,key,col,label=path.slice(path.lastIndexOf("/")+1)||"Home")=>
         (<li className={ "breadcrumb-item "+(key===col.length-1?"active":"") }
              aria-current="page"
              key={key}>
              {key===col.length-1
                ? label
                : <Link to={path}> {label} </Link>}
          </li>))}
</ol>
</nav>)

export default Breadcrumb
