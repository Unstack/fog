import React from "react";
import { Switch, Route, Redirect } from "react-router";

import { ToastContainer } from "react-toastify";

import Tabs      from "./tabs";

import Container from "../Container";

import pageset    from "../pageset";
import Error404   from "../pageset/404";

import mapIndexed from "../../util/mapIndexed";

const Game = ({ children, ...props }) => (
  <Container fluid className="p-0 m-0" id="game">
    <ToastContainer  pauseOnFocusLoss={false} pauseOnHover={false} />
    <Route path="/game" render={()=>
          <Tabs pathname={props.location.pathname}
              location={props.derived.location}
              player={props.player}
              className="mt-3 mb-3"/>
      }/>

    <Switch>
      {mapIndexed(
        (route, key) => (
          <Route
            key={key}
            path={route.path}
            exact={route.exact}
            render={routeinfo =>
              route.component({ ...props, ...routeinfo })
            }
          />
        ),
        pageset
      )}
      <Route exact path="/" render={() => <Redirect to="/home" />} />
      <Route path="*" render={Error404} />
    </Switch>

  </Container>
);

export default Game;
