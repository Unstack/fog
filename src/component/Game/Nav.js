import React from 'react'

import Container from '../Container'
import Link      from '../Link'

import heartIcon from '../../icon/heart.svg'
import sunIcon from '../../icon/sun.svg'
import dropletIcon from '../../icon/droplet.svg'
import starIcon from '../../icon/star.svg'

import logo from '../../logo.svg'

const GameNav = ({player:{character,activity},derived:{location:{name:locationName},area:{name:areaName}},push,className}) => (
  <Container fluid className="p-0 m-0">
  <nav className={"navbar navbar-light "+
                  "d-none d-sm-block navbar-expand-lg "+
                  "bg-primary animated slideInDown "+
                  className} id="navbar">
    <div className="navbar-brand">
    <Link to="/game">
      <img src={logo}
               alt="logo"
               className="animated bounceInLeft delay-1s"
               style={{width:"6vw",height:"auto"}}/>
    </Link>
    </div>

      <h1 className="h3 p-0 m-2 navbar-text">
        <Link to={"/game/character"}>{character.name}</Link>
        <span className="d-none d-lg-inline animated fadeIn">
          <span> @ </span>

            <Link to={"/game/world"}>{areaName}</Link>
            /
            <Link to={"/game/location"}>{locationName}</Link>
        </span>
      </h1>
  </nav>
    <div className="fixed-bottom d-flex flex-row justify-content-around bg-primary">
                <div className="text-center p-2">
                  <img src={heartIcon}
                       className="m-1"
                       alt="health"
                       style={{ width:"1em",
                                height:"auto",
                                path:{fill:"red"} }} />
                  {character.health.current/character.health.max*100}%
                </div>

                <div className="text-center p-2">
                  <img src={starIcon}
                       className="m-1"
                       alt="mana"
                       style={{width:"1em",height:"auto"}} />
                  {character.mana.current/character.mana.max*100}%
                </div>

                <div className="text-center p-2">
                <img src={dropletIcon}
                       className="m-1"
                       alt="thirst"
                       style={{width:"1em",height:"auto"}} />
                  {(character.thirst.current/character.thirst.max*100)-100}%
                </div>

                <div className="text-center p-2">
                  <img src={sunIcon}
                       className="m-1"
                       alt="hunger"
                       style={{width:"1em",height:"auto"}} />
                  {(character.hunger.current/character.hunger.max*100)-100}%
                </div>
    </div>
  </Container>
)

export default GameNav
