import React from 'react'

import Link      from "../Link";

const Tabs = ({pathname,player:{activity},className}) => (
  <ul className={"nav nav-pills nav-justified "+className}>
          <li className="nav-item m-1">
            <Link className={pathname.toLowerCase() === "/game"
                              ?"btn btn-outline-primary btn-lg active"
                              :"btn btn-outline-primary btn-lg "} to="/game">Overview</Link>
          </li>
          <li className="nav-item m-1">
            <Link className={pathname.toLowerCase().startsWith("/game/character")
                              ?"btn btn-outline-primary btn-lg active"
                              :"btn btn-outline-primary btn-lg"} to="/game/character">Character</Link>
          </li>
          <li className="nav-item m-1">
            <Link className={pathname.toLowerCase().startsWith("/game/world")
                              ?"btn btn-outline-primary btn-lg active"
                              :"btn btn-outline-primary btn-lg"} to="/game/world">World</Link>
          </li>
          <li className="nav-item m-1">
            <Link className={pathname.toLowerCase().startsWith("/game/activity")
                              ?"btn btn-outline-primary btn-lg active"
                              :"btn btn-outline-primary btn-lg"}
                              to="/game/activity">
                {activity ? activity.name : "Idle"}
            </Link>
          </li>
        </ul>)

export default Tabs
