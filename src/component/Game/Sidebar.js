import React from 'react'

import LinkSet   from '../LinkSet'
import Container from '../Container'
import Link      from '../Link'

import $ from 'jquery'

import defaultLocation  from '../../constant/model/location'
import defaultArea      from '../../constant/model/area'
import defaultPlayer    from '../../constant/model/player'
import defaultCharacter from '../../constant/model/character'

const Sidebar = ({ world,
                   player:  {character=defaultCharacter},
                   derived: {area=defaultArea,location=defaultLocation},
                   className,id}) => (
  <Container className={className} id={id}>
    <h1 className="h2">Menu</h1>
    <hr/>
    <Container.Row>
      <Container.Column>
        <LinkSet set={ [
                      {to:"/game",           label:"Overview"},
                      {to:"/game/status",    label:"Status"},
                      {to:"/game/journal",   label:"Journal"},
                      {to:"/game/world",     label:"World"},
                      {to:"/game/loadout",   label:"Loadout"}
                     ] }/>
        <hr/>
        <h1 className="h4">
          <Link to={"/game/location/"+character.position[1]} >
            { location.name }
          </Link>
        </h1>
        <LinkSet
            set={ area
                  .locations
                  .map((otherlocation,id)=>
                        (id===character.position[1]
                          ? null
                          : {to:"/game/location/"+id,
                              label:otherlocation.name})).filter((e)=>e) }
            fallbackText="No nearby locations" />
      </Container.Column>
    </Container.Row>
  </Container>
)

Sidebar.defaultProps = {
  world:[],
  player:defaultPlayer,
  derived:{area:defaultArea,location:defaultLocation}
}

export default Sidebar
