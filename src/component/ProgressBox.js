import React from 'react'

const ProgressBox = ({current,max,title,
                      color,
                      coloredWarning}) => (
   <div className="list-group-item">
          <span>{title}</span>
          <span className="float-right"> {current}/{max} </span>
            <div className="progress">
              <div className={"progress-bar "+(coloredWarning ? (current/max<=0.25
                                               ? "bg-danger"
                                               : (current/max <= 0.5
                                                  ? "bg-warning text-muted"
                                                  : "bg-success")) : color) }
                    role="progressbar"
                    style={{"width":(current/max*100)+"%" }}
                    aria-valuenow={current}
                    aria-valuemin="0"
                    aria-valuemax={max}> {Math.round(current/max*100)} %</div>
            </div>
  </div>
)

ProgressBox.defaultProps = {
  current: 0,
  max: 0,
  title: "Progress Box",
  color:"bg-success",
  coloredWarning:false
}

export default ProgressBox
