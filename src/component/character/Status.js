import React from 'react'

import ProgressBox from '../ProgressBox'

const CharacterStatus = ({character:{health,mana,hunger,thirst},className}) => (
  <div className={"list-group "+className}>
    <ProgressBox title="Health" {...health} coloredWarning />
    <ProgressBox title="Mana"   {...mana  } color="bg-color-purple" />
    <ProgressBox title="Hunger" {...hunger} color="bg-color-orange" />
    <ProgressBox title="Thirst" {...thirst} color="bg-color-blue" />
  </div>)

export default CharacterStatus
