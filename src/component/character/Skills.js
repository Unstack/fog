import React from 'react'

import mapIndexed from '../../util/mapIndexed'

const CharacterSkills = ({skills,className}) =>
  (<div className={"list-group "+className}>
    {mapIndexed(({name,description,
                    level,exp,tnl},key)=>(
                      <div className="list-group-item" key={key}>
                        <span>{name}</span>
                        <span className="float-right">{level}</span>
                        <div className="progress">
                          <div className="progress-bar text-dark bg-primary"
                                role="progressbar"
                                style={{"width":(exp/tnl[level]*100)+"%" }}
                                aria-valuenow={exp}
                                aria-valuemin="0"
                                aria-valuemax={tnl[level]}>
                                  {exp}/{tnl[level]} ({exp/tnl[level]*100}%)
                          </div>
                        </div>
                        <p className="float-sm-right mb-0 d-none d-lg-block">{description}</p>
                      </div>),skills)}
  </div>)

CharacterSkills.defaultProps = {
  skills:[]
}

export default CharacterSkills
