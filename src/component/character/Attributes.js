import React from 'react'

import mapIndexed from '../../util/mapIndexed'

const CharacterAttributes = ({attributes,className}) =>
  (<div className={"list-group "+className}>
          {mapIndexed(({name,value},key)=>(<div className="list-group-item" key={key}>
                    <span> {name}   </span>
                    <span className="float-right"> {value} </span>
                  </div>), attributes) }
  </div>)

CharacterAttributes.defaultProps = {
  attributes:[]
}

export default CharacterAttributes
