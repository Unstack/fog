import React from 'react'

import Status      from './Status'
import Skills      from './Skills'
import Attributes  from './Attributes'

import Container from '../Container'

import defaultCharacter from '../../constant/model/character'
import defaultSkills    from '../../constant/list/skills'

const CharacterInfo = ({character, skill,className}) =>
  (<Container className={className} fluid>
    <Status     character={character} />
    <Container.Row className="mt-4">
      <Container.Column>
        <Attributes attributes={character.attributes} />
      </Container.Column>
      <Container.Column>
        <Skills     skills={skill} />
      </Container.Column>
    </Container.Row>
  </Container>)

CharacterInfo.defaultProps = {
  character:  defaultCharacter,
  skills:     defaultSkills
}

export default CharacterInfo

