import React from 'react'

import {reduce,flatten} from 'ramda'

import LinkSet   from '../LinkSet'
import Container from '../Container'

import mapIndexed from '../../util/mapIndexed'

const LocationActions = ({locationId,action,
                          location:{type},
                          player:{skill=[]},className}) =>
    (<Container.Column className={className}>
      <h1 className="h4">Actions</h1>
        <LinkSet set={
          reduce(
            (col,playerskill)=>(
              [ ...col,
                ...flatten(playerskill.actions
                              .filter( (actionset)=> (actionset[0]<=playerskill.level) )
                              .map( (actionset)=>(
                                mapIndexed( ({name,activity}) => ({ to:"activity.start",
                                                                    args: [activity],
                                                                    label:name }),
                                          actionset
                                          .slice(1)
                                          .filter(
                                                  ({requires}) => (requires===type)
                                                 ) )
                              )))
              ]
            ),
            [],
          skill)
        }
        fallbackText="Nothing to do here"
        action={action}
        className="m-3"
        linkStyle="btn btn-block" />
    </Container.Column>)

export default LocationActions
