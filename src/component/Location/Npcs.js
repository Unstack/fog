import React from 'react'

import LinkSet   from '../LinkSet'
import Container from '../Container'

import mapIndexed from '../../util/mapIndexed'

const LocationNpcs = ({npcs=[],locationId,className}) => (
  <Container.Column className={className}>
    <h1 className="h4">Others</h1>
    <LinkSet set={ mapIndexed((npc,id)=> ({
              to:"/game/location/"+locationId+"/npc/"+id,
              label: npc.name
            }), npcs ) }
            fallbackText="Nobody of interest" />
  </Container.Column>
)

export default LocationNpcs
