import React from 'react'

import LinkSet   from '../LinkSet'
import Container from '../Container'

import mapIndexed from '../../util/mapIndexed'

const LocationBuildings = ({buildings=[],locationId,className}) => (
  <Container.Column className={className}>
    <h1 className="h4">Buildings</h1>
    <LinkSet set={ mapIndexed((building,id)=> ({
              to:"/game/location/"+locationId+"/building/"+id,
              label: building.name
            }), buildings) } fallbackText="Nothing of interest" />
  </Container.Column>
)

export default LocationBuildings
