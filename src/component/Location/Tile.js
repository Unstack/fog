import React from 'react'

import Container from '../Container'

const locationColors = {
  "LOCATION_BEACH":"bg-color-blue",
  "LOCATION_FIELD":"bg-color-green",
  "LOCATION_TOWN":"bg-color-purple",
  "LOCATION_DUNGEON":"bg-dark",
  "LOCATION_LAKE":"bg-info",
  "LOCATION_FOREST":"bg-color-teal",
  "LOCATION_CAMP":"bg-color-indigo",
  "LOCATION_HOME":"bg-color-pink"
}


const LocationTile = ({area,
                   location,
                   areaid,
                   locationid,
                   push,
                   className,current}) => (
    <Container fluid className={className}>
        <Container.Row>
          <Container.Column className={locationColors[location.type]}>
            <p className="text-light text-center mt-4 p-4">
              {location.type.slice(location.type.indexOf("_")+1)}
            </p>
          </Container.Column>
          <Container.Column>
            <h1 className="h3">{location.name}</h1>
            <p>{location.description||"This location is missing a description"}</p>
          </Container.Column>

          {push &&
          <button className={ "btn btn-block rounded-0 "+
                              (current&&"btn-primary")}
                  onClick={ ()=>( push("/game/world/"+areaid+"/"+locationid)) } >
                  {current?"Return":"View"}
          </button> }
        </Container.Row>

    </Container>
)

LocationTile.defaultProps = {
  current:false
}
export default LocationTile
