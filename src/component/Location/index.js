import React from 'react'

import LocationTile      from './Tile'

import LocationBuildings from './Buildings'
import LocationNpcs      from './Npcs'
import LocationActions   from './Actions'
import NearbyLocations   from './Nearby'

import Container from '../Container'

const Location = ({ area, areaid,
                    location,locationid,
                    player, action}) => (
  <Container>
    <Container.Row>
      <LocationTile area={area}
                    location={location}
                    areaid={areaid}
                    locationid={locationid} />
    </Container.Row>
    <hr/>
    <Container.Row>
    <LocationActions location={location}
                     locationId={locationid}
                     player={player}
                     action={action}
                    className="col-12" />
      <LocationBuildings buildings={location.buildings} locationId={locationid} />
      <LocationNpcs npcs={location.npcs} locationId={locationid} />
   </Container.Row>
  </Container>
)

export default Location
