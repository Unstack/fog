import React from 'react'

import LinkSet   from '../LinkSet'
import Container from '../Container'

import mapIndexed from '../../util/mapIndexed'

const NearbyLocations = ({location, area, areaid, className}) => (
  <Container.Column className={className}>
     <h1 className="h4">Nearby</h1>
    <LinkSet set={mapIndexed((location)=>({
      to:"/game/world/"+areaid+"/"+area.locations.indexOf(location),
      label:location.name
    }), area.locations.filter((e)=>e!==location) )} vertical />
  </Container.Column>
)

export default NearbyLocations
