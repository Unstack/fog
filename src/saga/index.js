import {all} from 'redux-saga/effects'

import gameSaga     from './game'
import routerSaga   from './router'

function * rootSaga () {
  yield all([
      gameSaga(),
      routerSaga()
  ])
}
export default rootSaga
