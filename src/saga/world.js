import { delay } from 'redux-saga'
import { put, call } from 'redux-saga/effects'
import {push} from 'connected-react-router'

import mapIndexed from '../util/mapIndexed'

import jsf      from 'json-schema-faker'
import schema   from '../constant/schema'

import world     from '../action/world'
import loader    from '../action/loader'
import character from '../action/character'
import locations from '../constant/location'

const tutorialStory = ({name,fog,locations:areaLocations}) => ({
  name,fog,locations:areaLocations.slice(0,3).concat( locations.camp )
})

const story = {
  "12": tutorialStory
}

const storyPerArea = (area,id) => (
  story[id.toString()]
    ? story[id.toString()](area)
    : area
)

const applyStory = (world) => ( mapIndexed(storyPerArea, world) )

function * worldGenerateSaga() {
  yield put( loader.toggle() )
  const time = performance.now()

  const newWorld = yield call(jsf.resolve, schema.world, schema)
  const newWorldWithStory = yield call(applyStory,newWorld)

  const duration = Math.round(performance.now() - time)

  //Content flash prevention
  console.log("Took "+duration+"ms to generate a new world")
  yield call( delay, 3100-duration )
  yield  put( world.load(newWorldWithStory) )
  yield  put( character.go(12, newWorldWithStory[12].locations.length-1) )
  yield  put( push("/game") )
  yield  put( loader.toggle() )
}

export default {
  worldGenerateSaga
}
