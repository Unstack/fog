import { put,call} from 'redux-saga/effects'

import faker       from 'faker'
import Chance      from 'chance'

import jsf          from 'json-schema-faker'

import character    from '../action/character'

import locationType from '../constant/list/locationType'

const chance = new Chance()

/*const activities = {
    "LOCATION_TOWN":{
      a:1
    }
}*/

jsf.extend('faker',  () => faker  )
jsf.extend('chance', () => chance )
jsf.extend('fog',    () => ({
  location:{
    type:({weights}) => ( chance.weighted(locationType, weights ) ),
    provides: ({type,size}) => ( ["POO"] )
  }
}) )

function * characterRandomizerSaga({property, generator, kind, args}) {
  var randomizedProperty;
  if (generator==="faker")
  {
        const [kind_class,kind_type] = kind.split(".")
        randomizedProperty = yield call( faker[kind_class][kind_type],args )
  }
  else
      randomizedProperty = chance[kind].apply(chance,args)

  yield put(character.set.property(property,randomizedProperty))
}

export default {
  characterRandomizerSaga
}
