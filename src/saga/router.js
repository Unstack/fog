import {fork, put, select}       from 'redux-saga/effects'
import {router}                  from 'redux-saga-router'
import {push}                    from 'connected-react-router'

import character from '../action/character'

import history   from '../util/history'

function * debugHandlerSaga(){

}

function * pageHandlerSaga ({page="home",...params}) {
  yield put(character.set.random("name","faker","name.findName"))
}

function * gameHandlerSaga ({page="overview",...params}) {
  const {game:{world}} = yield select()
  if (world === null)
      return yield put(push("/home"))
}

const routes = [
   {pattern:"/debug",                      handler:debugHandlerSaga},
   {pattern:"/game/:page?/:id?/:build?",   handler:gameHandlerSaga},
   {pattern:"/:page?",                     handler:pageHandlerSaga}
]

function * routerSaga() {
  yield fork(router, history, routes)
}

export default routerSaga
