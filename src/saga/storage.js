import { delay }                from 'redux-saga'
import { put, call,select }    from 'redux-saga/effects'
import {toast}                  from 'react-toastify'

import store2 from 'store2'

import $ from 'jquery'

import about from '../constant/about'

import loader    from '../action/loader'
import world     from '../action/world'
import character from '../action/character'
import skill     from '../action/skill'
import quest     from '../action/quest'

import activity  from '../action/activity'

window.store2 = store2

function * gameLoadSaga() {
  const savedata = yield call(store2, about.name)
  if (savedata === null)
    return yield put ( loader.ready() )
  console.log(savedata)
  yield put( world.load(savedata.world)   )
  yield put( character.load(savedata.player.character) )
  yield put( skill.load(savedata.player.skill) )
  yield put( quest.load(savedata.player.quest) )

  if (savedata.player.activity )
    yield put( activity.continue(savedata.player.activity) )

  return yield put ( loader.ready() )

}

function * gameSaveSaga({initial}) {
  const {game:gameData} = yield select()
  yield call(store2, about.name, gameData)
  yield call(toast,"Game saved!")
}

function * gameResetSaga() {
  yield call(toast, "Oh, you're leaving... ")

  yield call(store2, about.name, null)
  window.nuked = true

  yield call(delay,2000)

  yield call(toast,"...We hope you enjoyed the Fog :( ")

  yield call(delay,1000)

  $("#content").fadeOut(3000,()=> ( window.location.assign("/home" ) ) )
}

export default {
  gameLoadSaga,
  gameSaveSaga,
  gameResetSaga
}
