import { delay }                      from 'redux-saga'
import { put,takeLatest,takeEvery,
         call, select,fork }          from 'redux-saga/effects'

import { toast }                      from 'react-toastify'

import store from '../store'
import store2 from 'store2'

import worldSaga    from './world'
import storageSaga  from './storage'
import fakerSaga    from './faker'

import GAME      from '../constant/action/game'
import TOAST     from '../constant/action/toast'
import ACTIVITY  from '../constant/action/activity'
import CHARACTER from '../constant/action/character'
import WORLD     from '../constant/action/world'

import about       from '../constant/about'

import world       from '../action/world'
import activity    from '../action/activity'
import intro       from '../action/intro'

function * toastSaga ({text="You forgot to pass a text to this toast!",options}) {
  yield call(toast,text,options)
}

function * activitySaga({activity:{name,duration}}) {
  yield call(toast,name,{autoClose:duration})
  yield call(delay,duration)
  yield call(toast,"Done!")
  yield put(activity.stop())
}

function * newGameSaga () {

  const {game:{player:{character:{name, gender}}}} = yield select()

  if (name.length < 3)
    return yield call(toast,"Character name needs to be at least 4 characters long")
  else if (name.length > 24)
    return yield call(toast,"Character name can be at most 24 characters long")
  else if (gender === -1)
      return yield call(toast,"Gender DLC Pack not installed - Buy now for only $29,99")

  yield put( intro() )
  yield put( world.generate() )
}

function * gameSaga () {
  yield takeEvery(  TOAST.CREATE,   toastSaga    )

  yield takeLatest( GAME.NEW,          newGameSaga  )
  yield takeLatest( ACTIVITY.START,    activitySaga )

  yield takeLatest( GAME.LOAD,     storageSaga.gameLoadSaga     )
  yield takeLatest( GAME.SAVE,     storageSaga.gameSaveSaga     )
  yield takeLatest( GAME.RESET,    storageSaga.gameResetSaga    )

  yield takeLatest( CHARACTER.SET.RANDOM, fakerSaga.characterRandomizerSaga)

  yield takeLatest(WORLD.GENERATE, worldSaga.worldGenerateSaga)

  window.onbeforeunload = () => {
    if (window.nuked)
      return
    store2(about.name, store.getState().game)
    console.log("saved")
    }

  yield fork(storageSaga.gameLoadSaga)
}


export default gameSaga


